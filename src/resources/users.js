import Vue from 'vue'
import VueResource from 'vue-resource'
Vue.use(VueResource)

var resource = Vue.resource('https://api.github.com/users');

export {resource as default}

