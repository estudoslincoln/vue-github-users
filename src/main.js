import Vue from 'vue'
import VueRouter from 'vue-router'
import VueResource from 'vue-resource'
import App from './App.vue'
import Home from './components/Home.vue'
import Users from './components/Users.vue'

Vue.use(VueRouter)
Vue.use(VueResource)

var router = new VueRouter()

// Pointing routes to the components they should use
router.map({
    '/home': {
        component: Home
    },
    '/users': {
        component: Users
    },
    '/test': {
        component: Users
    }

})

router.redirect({
    '*': '/home'
})

router.start(App, '#app')